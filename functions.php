<?php
/**
 * The main function definitions for the Dallas Fair theme
 * @version 0.1
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You do not have permission to access this file directly.' );
}

if ( ! class_exists( 'Dallas_Fair' ) ) {
	class Dallas_Fair {
		/**
		 * Holds the version number for use with various assets
		 *
		 * @since  1.0.1
		 * @access public
		 * @var    string
		 */
		public $version = '0.1';
		
		/**
		 * Holds the class instance.
		 *
		 * @since   1.0.1
		 * @access	private
		 * @var		Dallas_Fair
		 */
		private static $instance;
		
		/**
		 * Returns the instance of this class.
		 *
		 * @access  public
		 * @since   1.0.1
		 * @return	Dallas_Fair
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) ) {
				$className = __CLASS__;
				self::$instance = new $className;
			}
			return self::$instance;
		}
		
		/**
		 * Create the object and set up the appropriate actions
		 */
		private function __construct() {
			//* Start the engine
			include_once( get_template_directory() . '/lib/init.php' );
			
			//* Setup Theme
			include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );
			
			//* Add Image upload and Color select to WordPress Theme Customizer
			require_once( get_stylesheet_directory() . '/lib/customize.php' );
			
			//* Include Customizer CSS
			include_once( get_stylesheet_directory() . '/lib/output.php' );
			
			//* Child theme (do not remove)
			define( 'CHILD_THEME_NAME', 'Dallas Fairs' );
			define( 'CHILD_THEME_URL', 'https://www.dallaspsychicfair.com/' );
			define( 'CHILD_THEME_VERSION', $this->version );
			
			add_action( 'wp_enqueue_scripts', array( $this, 'add_scripts_and_styles' ) );
			add_action( 'template_redirect', array( $this, 'template_redirect' ) );
			
			add_action( 'after_setup_theme', array( $this, 'adjust_genesis' ) );
			add_action( 'genesis_setup', array( $this, 'register_sidebars' ) );
			
			add_theme_support( 'genesis-footer-widgets', 3 );
		}
		
		/**
		 * Adjust anything within Genesis that needs to be modified
		 */
		function adjust_genesis() {
			remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
			add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );
			add_theme_support( 'genesis-responsive-viewport' );
			add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );
			add_theme_support( 'genesis-structural-wraps', array(
				'header',
				'nav',
				'subnav',
				'site-inner',
				'footer-widgets',
				'footer'
			) );
			//* Add support for custom background
			add_theme_support( 'custom-background' );
			//* Add support for custom header
			add_theme_support( 'custom-header', array(
				'width'           => 1140,
				'height'          => 164,
				'header-selector' => '.site-header .wrap',
				'header-text'     => false,
				'flex-height'     => true,
			) );
		}
		
		/**
		 * Register/enqueue any necessary style sheets/script files
		 */
		function add_scripts_and_styles() {
			wp_register_style( 'genesis', get_stylesheet_uri(), array(), $this->version, 'all' );
			wp_enqueue_style( 'dallas-fair', get_stylesheet_directory_uri() . '/dallas-fair.css', array( 'genesis' ), $this->version, 'all' );
		}
		
		/**
		 * Perform any changes that need to happen after we've identified the queried object
		 */
		function template_redirect() {
			return;
		}
		
		/**
		 * Register any necessary widgetized areas/sidebars
		 */
		function register_sidebars() {
			return;
		}
	}
}

global $dallas_fair;
$dallas_fair = Dallas_Fair::instance();