# Dallas Fairs

The WordPress theme used on the [Dallas Psychic Fair](https://dallaspsychicfair.com/) and [Dallas WellBeing Fair](https://dallaswellbeingfair.com/) websites.

## Version

0.1

## Parent Theme

This is a very basic child theme of the [Genesis Framework](http://studiopress.com/)

## Contributors

* Developed and maintained by: [Ten-321 Enterprises](http://ten-321.com/)
